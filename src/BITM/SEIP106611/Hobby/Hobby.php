<?php
namespace App\BITM\SEIP106611\Hobby;

use \App\BITM\SEIP106611\Utility\Utility;

class Hobby{
    
    public $id = "";
    public $name = "";
    public $hobby = "";
    
    public function __construct($data = false){
        if(is_array($data) && array_key_exists("id", $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        $this->name = trim($data['name']);
        $this->hobby = trim($data['hobby']);
    }
    
    public function index(){
        
        $hobbys = array();
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $query = "SELECT * FROM `hobby`";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_object($result)){
            $hobbys[] = $row;
        }
        return $hobbys;
    }
    
    
    public function store(){
       
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $checkboxs=$_POST['hobby'];
		$check="";
		foreach($checkboxs as $checkbox)  
		   {  
			  $check .= $checkbox.","." ";  
		   } 
		
		$query = "INSERT INTO `atomicproject`.`hobby` (`name`,`hobby`) VALUES ( '".$this->name."','".$check."')";
        $result = mysql_query($query);
        if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Inserted Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again...</div>");
        }
        
        Utility::redirect('index.php');
				
    }

    public function show($id = false){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        $query = "SELECT * FROM `hobby` WHERE id=".$id;
        $result = mysql_query($query);
        $row = mysql_fetch_object($result);
        return $row;

    }

    public function update(){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
		$checkboxs=$_POST['hobby'];
		$check="";
		foreach($checkboxs as $checkbox)  
		   {  
			  $check .= $checkbox.","." ";  
		   } 
		
		
        $query = "UPDATE `atomicproject`.`hobby` SET `name` = '".$this->name."', `hobby` = '".$check."' WHERE `hobby`.`id` = ".$this->id;

        $result = mysql_query($query);
               
        if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Updated Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again....</div>");
        }
        
        Utility::redirect('index.php');
    }

    public function delete($id = null){
       
        if(is_null($id)){
            Utility::message("<div class=\"message_error\">No id avaiable. Sorry !</div>");
            Utility::redirect('index.php');
        }
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");

        $query = "DELETE FROM `atomicproject`.`hobby` WHERE `hobby`.`id` = ".$id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Deleted Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again...</div>");
        }
        
        Utility::redirect('index.php');
    }

}
